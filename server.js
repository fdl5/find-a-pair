// Express.js Middleware API to handle requests, paths and resources

/**
 * Module dependencies.
 */
var path = require('path');
var express = require('express');
var app = express();
var request = require('request');
var bodyParser = require('body-parser');
var port = process.env['PORT'] || 3000;

module.exports = app;

// Static application files
app.use(express.static(__dirname + '/index.html'));
app.use(express.static(__dirname + '/resources/src/css/stylesheet.css'));
app.use(express.static(__dirname + '/resources/src/js/concat.js'));
app.use(express.static(__dirname + '/resources/src/data/fields.json'));

//Static vendor files
app.use(express.static(__dirname + '/node_modules/angular/angular.min.js'));
app.use(express.static(__dirname + '/node_modules/angular-animate/angular-animate.min.js'));
app.use(express.static(__dirname + '/node_modules/angular-messages/angular-messages.min.js'));
app.use(express.static(__dirname + '/node_modules/angular-mocks/angular-mocks.js'));
app.use(express.static(__dirname + '/node_modules/angular-formly/dist/formly.min.js'));
app.use(express.static(__dirname + '/node_modules/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.min.js'));
app.use(express.static(__dirname + '/node_modules/api-check/dist/api-check.min.js'));
app.use(express.static(__dirname + '/node_modules/express/lib/express.js'));
app.use(express.static(__dirname + '/node_modules/bootstrap/dist/css/bootstrap.min.css'));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Custom endpoints for the resources
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'))
});
app.get('/css', function (req, res) {
    res.sendFile(path.join(__dirname + '/resources/dest/css/stylesheet.css'))
});
app.get('/js', function (req, res) {
    res.sendFile(path.join(__dirname + '/resources/dest/js/concat.js'))
});
app.get('/bootstrap', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/bootstrap/dist/css/bootstrap.min.css'))
});
app.get('/icon', function (req, res) {
    res.sendFile(path.join(__dirname + '/resources/src/img/bbc.png'))
});
app.get('/express', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/express/lib/express.js'))
});
app.get('/angular', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/angular/angular.min.js'))
});
app.get('/angular-route', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/angular-route/angular-route.min.js'))
});
app.get('/angular-animate', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/angular-animate/angular-animate.min.js'))
});
app.get('/angular-messages', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/angular-messages/angular-messages.min.js'))
});
app.get('/angular-mocks', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/angular-mocks/angular-mocks.js'))
});
app.get('/formly', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/angular-formly/dist/formly.min.js'))
});
app.get('/formlyBootstrap', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.min.js'))
});
app.get('/apiCheck', function (req, res) {
    res.sendFile(path.join(__dirname + '/node_modules/api-check/dist/api-check.min.js'))
});
app.get('/data', function (req, res) {
    res.sendFile(path.join(__dirname + '/resources/data/fields.json'))
});

// Web server to listen to the assigned port
app.listen(port, function () {
    console.log('Express app listening on port: ' + port);
    console.log(__dirname);
});