## Find-A-Pair Application

This is a Single Page Application that demonstrates the ability to generate all possible programming pairs in a group of developers using an
arbitrary number of names entered by the user. A form builder was developed as part of this application, which is used to dynamically generate unique field elements for each name. 
Moreover, a custom validation mechanism was built that handles the validation for each field and provides error messages for the invalid fields.
The application stores the data temporary in the model object but since it is not persistent it loses all information when the application is reloaded or the Browser's page is refreshed.

The application logic and functionality was developed using Angular JS while Express JS is used as the middleware API solution that handles the resource files and provides
an extra layer of security for the HTTP asynchronous requests received from the client. THis improves the performance and reduces the load time required for the resources on the client's side. 
Grunt was used for the build automation and tasking where a variety of tasks were created for the purposes of uglifying files, run unit testing, create test coverage reports and others.
The development of the application followed a TDD approach with Jasmine being the selected Unit testing framework to run the tests of the application while Istanbul is the Coverage tool that provides metrics and a report in HTML format for the overall coverage of the JS files. 
As far as pre-processing of the styling is concerned, SASS partials were used that categorise the styling per category. These partials are combined into a single CSS stylesheet everytime the build process of the application is initiated.

The application is developed using the following Technical Stack:

```
ANGULAR 
EXPRESS
GRUNT
JASMINE
ISTANBUL
HTML
CSS3 
SASS 
BOOTSTRAP
```

### Prerequisites

The following packages need to be installed before running the application. Please execute the ***npm install*** command in the terminal or run the Grunt task ***Grunt Run*** that will handle the dependencies, concatenate, minify and uglify the JavaScript and SASS files. Once the process is complete please execute the ***Grunt Server*** task, which will run the application in a local static web server using Node/Express.

```
 Angular 1.6
 Angular Formly
 Angular Formly Templates Bootstrap
 Angular Messages
 Angular Animate
 Angular Mocks
 Express
 Request
 Api-check
 Grunt
 Grunt CLI
 Grunt-contrib-concat
 Grunt-contrib-uglify
 Grunt-contrib-jasmine
 Grunt-contrib-watch
 Grunt-contrib-parallel
 Grunt-Istanbul
 Grunt-express
 Grunt-parallel
 Grunt-sass
 Grunt-Template-Jasmine-Istanbul
 Bootstrap
 BodyParser
 ```

### Installation & Execution

 Please follow the steps below in order to run the application in a localhost environment.

 * Execute ***npm install*** command, which will download and install the necessary NPM packages
   Please note that the ***node_modules*** folder is also included in the repository/ZIP package in case of bad network connection to resolve the dependencies. Clone the repository using the following **git clone** command:
                      ```
                      https://fdl5@bitbucket.org/fdl5/find-a-pair.git
                      ```
   Alternatively, please download the zip and extract the files in your local storage.

 * Once the installation is completed, please execute the command ***npm start***, which will run the Grunt task ***Run***
   This Grunt task will concatenate, minify and uglify the JavaScript and CSS file and create two files (concat.js and stylesheet.css)
   in the destination folder (dest). In addition, the tasks jasmine and istanbul will be executed, which will run the unit tests and create a test coverage report. 
   Once this process is complete, please execute the Grunt task ***Server*** which will initiate a local webserver that will hosting the files using the custom shortlinks 
   included in the server.js file.

 * Please open a Browser and go to ***http://localhost:3000/*** and the index.html will be displayed. If the 3000 port is being used please refer to the log in the terminal in which the used port will be included.

### Entry point

 ***index.html***: This is the main index.html file that is hosted by the localhost. The resources of this HTML file are handled by  the middleware API and so JavaScript **should** be enabled in order to display all UI contents, fetch the fields and apply the application logic required for this task.
The source code has sufficient amount of comments, which further explain the functionality of the application.

### Authors

 **Alexios Fadl**