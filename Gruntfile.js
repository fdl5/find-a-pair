'use strict';

var path = require('path');

module.exports = function (grunt) {
    // The Angular application files (Main module, Controller and Service)
    var javascriptSrc = [
        '<%= src.paths.srcFolder %>/js/angular-app/app/pairApp.js',
        '<%= src.paths.srcFolder %>/js/angular-app/controllers/PairController.js',
        '<%= src.paths.srcFolder %>/js/angular-app/services/PairService.js'
    ];

    var sassSrc = [
        '<%= src.paths.srcFolder %>/<%= src.paths.sass %>'
    ];

    grunt.initConfig({

        // Link the source paths from the package JSON file
        src: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: '\n'
            },
            js: {
                src: javascriptSrc,
                dest: '<%= src.paths.destFolder %>/js/concat.js'
            }
        },
        // Uglification of JS code in order to reduce the file size
        uglify: {
            options: {
                banner: '<!-- Concatenated and Minified Javascript -->',
                sourceMap: true,
                mangle: false // Do not distort variable names -Debug
            },
            dist: {
                files: {
                    '<%= concat.js.dest %>': ['<%= concat.js.dest %>']
                }
            }
        },
        // Compilation of SASS files and output a css file
        sass: {
            options: {
                sourceMap: true
            },
            process: {
                src: sassSrc,
                dest: '<%= src.paths.destFolder %>/<%= src.paths.css %>/stylesheet.css'
            }
        },
        // Jasmine task
        jasmine: {
            all: {
                src: [
                    // The JavaScript files that will be tested
                    javascriptSrc
                ],
                // The dependencies
                options: {
                    'vendor': ['node_modules/angular/angular.js',
                        'node_modules/api-check/dist/api-check.js',
                        'node_modules/angular-formly/dist/formly.js',
                        'node_modules/angular-animate/angular-animate.js',
                        'node_modules/angular-messages/angular-messages.js',
                        'node_modules/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.js',
                        'node_modules/angular-mocks/angular-mocks.js'],
                    // The test directory
                    'specs': 'resources/test/*.js'
                }
            },
            istanbul: {
                src: '<%= jasmine.all.src %>',
                options: {
                    vendor: '<%= jasmine.all.options.vendor %>',
                    specs: '<%= jasmine.all.options.specs %>',
                    template: require('grunt-template-jasmine-istanbul'),
                    templateOptions: {
                        coverage: 'resources/test/coverage/json/coverage.json',
                        report: [
                            {type: 'html', options: {dir: 'resources/test/coverage/html/coverage.html'}},
                            {type: 'text-summary'}
                        ]
                    }
                }
            }
        },
        express: {
            defaults: {
                options: {
                    server: path.resolve('server.js'),
                    serverreload: true
                }
            }
        }
    });

    // Load the Grunt dependencies and Tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-express');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-watch');



    // Definition of the Grunt tasks

    // Default tasks to concatenate, uglify and minify JS and SASS/CSS3 files
    grunt.registerTask('Run', [
        'concat',
        'uglify',
        'sass',
        'jasmine:istanbul'
    ]);

    // Task to run a local web server using Express
    grunt.registerTask('Server', [
        'express'
    ]);

    // Tasks to run unit tests and test coverage
    grunt.registerTask('Test & Coverage', ['jasmine:istanbul']);

};