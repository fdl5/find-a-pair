/** Unit test of the Pair Service **/

describe('Testing the Pair Service', function () {

    var $httpBackend, PairService;

    // Executed before each test in in the describe
    beforeEach(function() {
        // Using the Angular Mocks capability that allows us to use the Angular module
        module('pairApp');
        // Inject the fake HTTP backend service that mocks the HTTP service used in the Controller and Service under test
        inject(function (_$httpBackend_, _PairService_) {
            // Assigninment
            $httpBackend = _$httpBackend_;
            PairService = _PairService_;
        });
        // Mock the http request to retrieve the field data. Response is successful with HTTP status code 200
        $httpBackend.when('GET', '/data').respond(200, 'success');
    });

    // Unit test to fetch data. Once the getData method is called and a promise is created the output is digested.
    it('should succesfully fetch the field from the JSON file', function () {
        var promise = PairService.getData();
        expect(promise).toBeDefined();
        $httpBackend.flush();
        $httpBackend.verifyNoOutstandingExpectation();
    })

});