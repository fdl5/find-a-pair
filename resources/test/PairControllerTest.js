/** Unit test of the Pair Controller **/

describe('Testing Pair Controller', function() {
    var $scope, controller, mockService, $log, $q, deferred;

    // Everything in this block will run before each test in the describe
    beforeEach(function () {

        // Initialize the Angular module
        module('pairApp');

        inject(function (_$rootScope_, _$controller_, _$log_, _$q_) {
            $scope = _$rootScope_.$new();
            $log = _$log_;
            $q = _$q_;
            deferred = _$q_.defer();

            mockService = {};

            mockService.getData = function () {
                var deferred = $q.defer();
                deferred.resolve($scope.pairFields);
                return deferred.promise;
            };

            controller = _$controller_('PairController', {
                PairService: mockService,
                $scope: $scope,
                $log: $log
            });
        });
    });

    it('Should call the function to Add new fields in the view', function () {
        $scope.pairFields = [];
        // Call function
        $scope.addField();
        // Digest the output
        $scope.$digest();
        // Expect the pairs not to be generated since no data has been passed from the model
        expect($scope.pairsGenerated).toBeFalsy();
    });

    it('Should call the  function to Remove the last field from the view', function () {
        // Initialising the variable for the last index and pair fields
        var lastIndex = {};
        $scope.pairFields = [];
        // Call the function
        $scope.removeField();
        //Digest
        $scope.$digest();
        // Expect the index to be resolved
        expect(lastIndex).toBeDefined();
    });
    it('Should call function to generate the pairs', function () {
        // Mock array with mock data
        var convertedArray = ["Name A", "Name B", "Name C"];
        // Mock model with mock data
        $scope.pairModel = {name: "Name A", name2: "Name D"};
        // Output array initialised
        $scope.namesList = [];
        // Call function
        $scope.generatePairs();
        $scope.$digest();
        $scope.hasDuplicates();
        $scope.$digest();
        // Expect duplicate helped function to return TRUE so the error will be shown
        expect($scope, hasDuplicates).toHaveBeenCalledWith(convertedArray);
        expect($scope, 'hasDuplicateError').toBe(true);
    });
});