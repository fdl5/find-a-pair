'use strict';

/** The Service retrieves the field data from the local JSON file using an HTTP request that calls the endpoint in the Express middleware API.
 * It is the Controller's responsibility to handle the request asychronously by creating a promise and resolving it once the data has been fetched successfully.
 */
angular.module('pairApp').service('PairService', [
    '$http',
    function ($http) {

        var self = this;

        // Create an HTTP request to fetch the data (fields) by calling the /data endpoint from the server.js file
        self.getData = function () {
            return $http({
                method: 'GET',
                url: '/data'
            })
        };
    }
]);
