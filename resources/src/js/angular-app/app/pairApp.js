'use strict';

/** The Angular module is initiated and the dependencies Angular Formly, Angular Animate and Angular Messages are injected.
 * In addition, the Formly Configuration Provider allows the application to create a custom validation for each field.
 * The validation is triggered every time a condition is met and it is applied for the Input field type.
 *
 * For the purposes of this task, the 'required' validation is added in order to dynamically display an error message under
 * the mandatory fields which are left empty.
 * **/
var pairApp = angular.module('pairApp', ['formly', 'formlyBootstrap', 'ngAnimate', 'ngMessages']);

// Validation for the fields. The validation is triggered when the form is invalid
pairApp.run(function (formlyConfig, formlyValidationMessages) {
    formlyConfig.extras.errorExistsAndShouldBeVisibleExpression = 'fc.$touched || form.$submitted';
    // Validation message that will be shown when the form is invalid. Each field has been set with the required flag
    // equal to TRUE, which indicates that all fields are mandatory to complete.
    formlyValidationMessages.addStringMessage('required', 'This field is required');
});

// Reference to the template URL in the view and the types that the validation supports (Input in our case)
pairApp.config(function (formlyConfigProvider) {
    formlyConfigProvider.setWrapper({
        name: 'validation',
        types: ['input'],
        templateUrl: 'error-messages.html'
    });

});
