'use strict';

/**
 * The Controller is used to build the application logic for the Find A Pair application.
 * A self invoking anonymous function fetches the first input field by calling the getData method from the Service.
 * Once the promise has been resolved, the data is passed to the pairFields object which is resposible for dynamically
 * displaying the fields in the view. The Controller also includes the functionality for adding and removing fields
 * to and from the fields object. Each field demonstrates a two-way binding model approach that captures the model changes
 * and dynamically displays them on the page once the corresponding event has occured .The model object conversion is also
 * part of the functionality so that the model object is converted into an array in order to generate all the possible pair programming combinations that will finally be displayed using
 * an ngRepeat directive to loop through each index of the array.
 *
 */
angular.module('pairApp').controller('PairController', [
    '$scope',
    '$log',
    'orderByFilter',
    'PairService',
    function ($scope, $log, orderByFilter, PairService) {

        var self = this;

        // Initialise the objects for the model and fields
        $scope.pairFields = {};
        $scope.pairModel = {};

        // The array in which the generated pairs will be added
        $scope.namesList = [];

        // Boolean flag that indicates if the pairs have been generated in order to display or not the button
        $scope.pairsGenerated = false;

        $scope.duplicateErrorMsg = 'Duplicate names are not allowed. Please checky our input and try again!';
        $scope.hasDuplicateError = false;

        // Self invoking function to fetch the field data from the json file
        (function () {
            // Create a promise that will return the data once the request has been resolved
            self.promise = PairService.getData().then(
                function (pairFields) {
                    // Add the fetched data in the fields array that will dynamically populate the fields in the view
                    $scope.pairFields = pairFields.data.fields;
                }
            )
                // Error handling
                ['catch'](function (pairFields) {
                $log.warn(pairFields.data + 'Error');
            })
        })();

        // Add new fields for additional names
        $scope.addField = function () {

            // Enable button in case a pair has been generated
            $scope.pairsGenerated = false;

            // Increase the array length and push the new field in the field array
            var newIndex = $scope.pairFields.length + 1;
            $scope.pairFields.push({
                "key": "name" + newIndex,
                "type": "input",
                "templateOptions": {
                    "label": "Name " + "(" + newIndex + ")",
                    "type": "text",
                    "required": "true",
                    "placeholder": "Please enter a name",
                    "minlength": 1,
                    "maxlength": 50
                }
            });
        };

        // Remove last name field
        $scope.removeField = function () {
            var lastIndex = $scope.pairFields.length - 1;
            $scope.pairFields.splice(lastIndex);
        };

        // This function converts the model object into an array and then generates all different pair combinations
        $scope.generatePairs = function () {

            // Resetting the duplicate indicator
            $scope.hasDuplicateError = false;

            // Convert model object into array and randomize the list
            if (typeof $scope.pairModel !== 'undefined') {
                var convertedArray = Object.keys($scope.pairModel).map(function (key) {
                    return $scope.pairModel[key];
                });

                // Check for duplicates. If there are no duplicates in the array, proceed with the pair generation
                if (($scope.hasDuplicates(convertedArray)) === false) {
                    // Get all pair combinations from the converted Array
                    for (var i = 0; i < convertedArray.length - 1; i++) {
                        // Capture the last value and push all each pair combination to the output array
                        for (var j = i + 1; j < convertedArray.length; j++) {
                            $scope.namesList.push(convertedArray[i] + ' and ' + convertedArray[j]);
                        }
                    }
                    // Disable the Pair generation button
                    $scope.pairsGenerated = true;
                }
                else {
                    // Show the validation error
                    $scope.hasDuplicateError = true;
                }
            }
        };

        // Helper function that checks for duplicate values in the array
        $scope.hasDuplicates = function (convertedArray) {
            var arrayLength = convertedArray.length;
            for (var i = 0; i < arrayLength; i++) {
                for (var j = i + 1; j < arrayLength; j++) {
                    // If there is a match return TRUE
                    if (convertedArray[i] == convertedArray[j]) {
                        return true;
                    }
                }
            }
            // Return false if there are no duplicates
            return false;
        }

    }]);